import React, { useState } from 'react';
import styled, {css} from 'styled-components';

import HeaderLogo from './HeaderLogo';
import MainNavButton from './MainNavButton';
import Menu from './Menu';

const menuOpenHeaderStyles = css`
    height: 100vh;
    width: 100vw;
`;

const Header = styled.header`
    ${props => props.menuOpen ? `${menuOpenHeaderStyles}` : ''}    
`;

const Nav = styled.nav`
    padding: 15px;
    display: flex;
    justify-content: space-between;
`;

const NavBar = (props) => {
  console.log(props);
  const [menuOpen, setMenuOpen] = useState(false);
  return (
    <>
        <Header>
            <Nav>
                <HeaderLogo />
                <MainNavButton menuOpen={menuOpen} onClick={() => setMenuOpen((prevMenuOpen) => !prevMenuOpen) }  />
            </Nav>
            <Menu open={menuOpen} closeMenu={() => setMenuOpen(false)} />
        </Header>
    </>
)};

export default NavBar;
