import React from 'react';
import { render } from '@testing-library/react';
import { createMemoryHistory } from 'history';
import { Router } from 'react-router-dom'

import NavBar from './';


const renderNavBar = () => {
    const history = createMemoryHistory()
    return render(
        <Router history={history}>
           <NavBar /> 
        </Router>
    );
}

describe('NavBar', () => {

    test('renders main navigation button', () => {
        const { getByLabelText } = renderNavBar();
        
        expect(getByLabelText('main navigation')).toBeVisible();
    });

    test('does documentation in menu show?', () => {
        const { getByText } = renderNavBar();
        
        expect(getByText('Documentation')).toBeVisible();
    });

});