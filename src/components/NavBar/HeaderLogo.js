import React from 'react';
import styled from 'styled-components';

import logo from './logo.svg';

const LogoLink = styled.a`
    width: 130px;
    height: 45px;
    display: block;
    z-index: 10;

    @media (min-width: 768px) {
        width: 185px;
        height: 62px;
    }
`;

const LogoImage = styled.img``;

const HeaderLogo = () => {
    return (
        <LogoLink href="/">
            <LogoImage src={logo}/>
        </LogoLink>
    );
};

export default HeaderLogo;