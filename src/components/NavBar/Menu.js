import React, { useState } from 'react';
import styled, {css} from 'styled-components';
import { NavLink } from 'react-router-dom';

const StyledMenu = styled.nav`
    display: flex;
    flex-direction: column;
    background: #fff;
    transform: ${({ open }) => open ? 'translateX(0)' : 'translateX(100%)'};
    height: 100vh;
    width: 100vw;
    text-align: left;
    position: absolute;
    top: 0;
    right: 0;
    z-index: 1;
    transition: transform 0.3s ease-in-out;

    margin-top: 75px;

    @media (min-width: 768px) {
        margin-top: 92px;
    }
`;

const topLevelMenuStyle = css`
    padding: 5px 15px;
    font-size: 2rem;
    color: #333;
    text-decoration: none;
    transition: color 0.3s linear;
    display: flex;
    justify-content: space-between;

    &:hover {
        color: #3455a5;
    }
`;

const TopLevelAnchor = styled.a`
    ${topLevelMenuStyle}
`;

const TopLevelNavLink = styled(NavLink)`
    ${topLevelMenuStyle}
`;

const TopLevelSubMenuTrigger = styled.button`
    ${topLevelMenuStyle};
    background: #fff;
    border: 0;
    font-weight: 100;
`;

const SubMenu = styled.nav`
    padding-left: 5px;
    display: ${props => props.visible ? 'block' : 'none'};
`;

const SubMenuNavLink = styled(NavLink)`
    padding: 5px 15px;
    font-size: 1rem;
    color: #333;
    text-decoration: none;
    transition: color 0.3s linear;
    display: flex;
    justify-content: space-between;

    &:hover {
        color: #3455a5;
    }
`

const SUB_MENU_STYLEGUIDE = 'STYLEGUIDE';

const Menu = ({ open, closeMenu }) => {
    const [subMenuItem, setSubMenuItem] = useState('');

  return (
      <StyledMenu open={open}>
          <TopLevelAnchor href="https://jss.sitecore.net"
          target="_blank"
          rel="noopener noreferrer">
              <span>Documentation</span>
              <span>&gt;</span>
          </TopLevelAnchor>
          <TopLevelSubMenuTrigger
          onClick={()=> {
            setSubMenuItem(prevState => {
                prevState === SUB_MENU_STYLEGUIDE ? 
                    setSubMenuItem('') : 
                    setSubMenuItem(SUB_MENU_STYLEGUIDE);
            }) 
          }}
            >
              <span>Styleguide</span>
              {
                  subMenuItem === SUB_MENU_STYLEGUIDE ?
                  <span>&or;</span> :
                  <span>&gt;</span>
              }
          </TopLevelSubMenuTrigger>
          <SubMenu visible={subMenuItem === SUB_MENU_STYLEGUIDE}>
            <SubMenuNavLink onClick={closeMenu} to="/styleguide#i63b0c99e-dac7-5670-9d66-c26a78000eae">
                Single-Line Text 
            </SubMenuNavLink>
            <SubMenuNavLink onClick={closeMenu} to="/styleguide#if1ea3bb5-1175-5055-ab11-9c48bf69427a">
            Multi-Line Text 
            </SubMenuNavLink>
            <SubMenuNavLink onClick={closeMenu} to="/styleguide#if166a7d6-9ec8-5c53-b825-33405db7f575">
                Date
            </SubMenuNavLink>
          </SubMenu>
          <TopLevelNavLink onClick={closeMenu} to="/graphql">
                <span>GraphQL</span>
              <span>&gt;</span>
          </TopLevelNavLink>
      </StyledMenu>
  )
}

export default Menu;