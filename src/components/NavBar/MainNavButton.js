import React from 'react';
import styled, {css} from 'styled-components';

const Button = styled.button`
    width: 44px;
    height: 44px;
    position: relative;
    border: 1px solid #f9f9f9;
    background: #fff;
    z-index: 10;
`;

const activeStyles = css`
    &:nth-child(1) {
        transform: rotate(45deg);
    }

    &:nth-child(6) {
        transform: rotate(45deg);
    }

    &:nth-child(2) {
        transform: rotate(-45deg);
    }

    &:nth-child(5) {
        transform: rotate(-45deg);
    }

    &:nth-child(1) {
        left: 5px;
        top: 9px;
    }

    &:nth-child(2) {
        left: calc(50% - 5px);
        top: 9px;
    }

    &:nth-child(3) {
        left: -50%;
        opacity: 0;
    }

    &:nth-child(4) {
        left: 100%;
        opacity: 0;
    }

    &:nth-child(5) {
        left: 5px;
        top: 27px;
    }

    &:nth-child(6) {
        left: calc(50% - 5px);
        top: 27px;
    }
`;

const ButtonIconPart = styled.span`
    display: block;
    position: absolute;
    height: 9px;
    width: 50%;
    background: #3455a5;
    opacity: 1;
    transform: rotate(0deg);
    transition: .25s ease-in-out;
    z-index: 10;

    &:nth-child(even) {
        left: 50%;
        border-radius: 0 9px 9px 0;
    }

    &:nth-child(odd) {
        left:0px;
        border-radius: 9px 0 0 9px;
    }

    &:nth-child(1) {
        top: 0px;
    }

    &:nth-child(2) {
        top: 0px;
    }

    &:nth-child(3) {
        top: 18px;
    }

    &:nth-child(4) {
        top: 18px;
    }

    &:nth-child(5) {
        top: 36px;
    }

    &:nth-child(6) {
        top: 36px;
    }

    ${props => props.active ? css`${activeStyles}` : ''}
`;

const MainNavButton = ( { menuOpen, onClick} ) => {
    return (
        <Button aria-label="main navigation" onClick={onClick}>
            <ButtonIconPart active={menuOpen} />
            <ButtonIconPart active={menuOpen}/>
            <ButtonIconPart active={menuOpen}/>
            <ButtonIconPart active={menuOpen}/>
            <ButtonIconPart active={menuOpen}/>
            <ButtonIconPart active={menuOpen}/>
        </Button>
    );
};

export default MainNavButton;