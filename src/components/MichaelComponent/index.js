import React from 'react';
import { Text } from '@sitecore-jss/sitecore-jss-react';
import styled from 'styled-components';

const NiceHeading = styled.h2`
  font-size: 20px;
  color: red;
`;

const MichaelComponent = (props) => {
  console.log(props);
  return (
  <>
    <NiceHeading>
      Name: <Text field={props.fields.Name} />
    </NiceHeading>
    <div>
    Age: <Text field={props.fields.Age} />
    </div>
  </>
)};

export default MichaelComponent;
